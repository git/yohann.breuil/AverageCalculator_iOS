# Average calculator iOS app

## 🎯 Goal

Create average calculator application with MVVM pattern in SwiftUI

## 📱 Views

| Home Page | UE View | UE Edit |
| -- | -- | -- |
| <img src="./.github/home-page.png" width="225" height="500"> | <img src="./.github/ue-view.png" width="225" height="500"> | <img src="./.github/ue-edit.png" width="225" height="500"> | 

## ⚙️ Functionalities

* Change UE note
* Edit a UE
* Edit a matiere

## 👨‍💻 Author 

**BREUIL Yohann**

* GitHub: [@DJYohann](https://github.com/DJYohann)
* LinkedIn: [@BREUIL Yohann](https://www.linkedin.com/in/yohann-breuil-02b18a165/)
