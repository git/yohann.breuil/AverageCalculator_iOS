import Foundation

import SwiftUI
import Stub
import ViewModel

struct MatiereInfo: View {
    @StateObject var matiereVM: MatiereVM
    
    var canEdit: Bool
    
    var body: some View {
        VStack {
            HStack() {
                Text("\(matiereVM.name)")
                Spacer()
                Text("\(matiereVM.factor)")
            }
            MatiereNoteSlider(matiereVM: matiereVM, canEdit: canEdit)
            Divider()
        }
    }
}

struct MatiereInfo_Previews: PreviewProvider {
    static var previews: some View {
        let odin = OdinVM(blocs: Stub().loadBlocs())
        let matiereVM = odin.blocsVM[0].uesVM[0].matieresVM[0]
        
        MatiereInfo(matiereVM: matiereVM, canEdit: true)
        //NoteInfo(canEdit: false)
    }
}
