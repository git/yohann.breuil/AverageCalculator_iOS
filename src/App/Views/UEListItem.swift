import SwiftUI
import Stub
import ViewModel

struct UEListItem: View {
    @StateObject var odin: OdinVM
    
    var body: some View {
        NavigationStack {
            VStack(alignment: .leading) {
                Label("UEs", systemImage: "doc.fill")
                    .font(.title)
                Text("Détails des UEs")
                LazyVStack {
                    ForEach(odin.blocsVM) { blocVM in
                        ForEach(blocVM.uesVM) { ueVM in
                            HStack {
                                UEInfo(ueVM: ueVM)
                                NavigationLink(destination: UEView(ueVM: ueVM)) {
                                    Image(systemName: "square.and.pencil")
                                }
                                Divider()
                            }
                        }
                    }
                }
            }
            .padding()
            .background(Color("ListItemBackgroundColor"))
            .cornerRadius(12)
        }
    }
}

struct UEListItem_Previews: PreviewProvider {
    static var previews: some View {
        let odinVM = OdinVM(blocs: Stub().loadBlocs())
        
        UEListItem(odin: odinVM)
    }
}
