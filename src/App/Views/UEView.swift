import SwiftUI
import Stub
import ViewModel

struct UEView: View {
    @StateObject var ueVM: UEVM
    
    @State private var canEditNote = false
    
    var body: some View {
        NavigationStack {
            ScrollView {
                UEInfo(ueVM: ueVM)
                VStack(alignment: .leading) {
                    Label("Coefficcient : \(ueVM.factor)", systemImage: "xmark.circle.fill")
                    Label("Détails des notes ", systemImage: "note.text")
                }
                .padding()
                
                LazyVStack {
                    ForEach(ueVM.matieresVM) { matiereVM in
                        HStack {
                            Button(action: {
                                canEditNote.toggle()
                            }) {
                                Image(systemName: canEditNote ? "lock.open" : "lock")
                            }
                            MatiereInfo(matiereVM: matiereVM, canEdit: canEditNote)
                        }
                    }
                }
                
            }
            .padding()
            .navigationTitle("UE\(ueVM.number) \(ueVM.name)")
            .toolbar {
                ToolbarItem {
                    Button(action: {
                        ueVM.isEditing.toggle()
                    }) {
                        Text("Edit")
                    }
                }
            }
            .sheet(isPresented: $ueVM.isEditing) {
                UEEdit(ueEditVM: ueVM)
            }
        }
    }
}

struct UEView_Previews: PreviewProvider {
    static var previews: some View {
        let odin = OdinVM(blocs: Stub().loadBlocs())
        let ueVM = odin.blocsVM[0].uesVM[0]
        
        UEView(ueVM: ueVM)
    }
}
