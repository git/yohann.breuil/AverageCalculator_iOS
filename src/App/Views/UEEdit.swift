import SwiftUI
import ViewModel
import Stub

struct UEEdit: View {
    @ObservedObject var ueEditVM : UEVM
    
    @State var newMatiereName = ""
    
    @State var number: Int32 = 2
    
    var body: some View {
        NavigationStack {
            Form {
                Section(header: Text("UE\(String(ueEditVM.number))")) {
                    VStack(alignment: .leading) {
                        Text("Nom").font(.headline)
                        TextField("Nom", text: $ueEditVM.name)
                    }
                    
                    VStack(alignment: .leading) {
                        Text("Coefficient").font(.headline)
                        TextField("Coefficient",value: $ueEditVM.factor, format: .number)
                            .keyboardType(.numberPad)
                    }
                }
                
                Section(header: Text("Matières")) {
                    ForEach($ueEditVM.matieresVM) { $matiereVM in
                        VStack {
                            TextField("Nom", text: $matiereVM.name)
                            TextField("Coeeficient", value: $matiereVM.factor, format: .number)
                                .keyboardType(.numberPad)
                        }
                        
                        .swipeActions(edge:.trailing){
                            Button(role: .destructive) {
                                
                            } label: {
                                Label("Delete", systemImage: "trash")
                            }
                        }
                    }
                    HStack {
                        VStack {
                            TextField("Nouvelle matière", text: $newMatiereName)
                            TextField("Nouveau coefficient", text: $newMatiereName)
                        }
                        Button(action: {}) {
                            Image(systemName: "plus.circle.fill")
                        }
                    }
                }
            }
            .navigationBarTitleDisplayMode(.inline)
            .navigationTitle("UE\(String(ueEditVM.number))")
            .toolbar {
                ToolbarItem(placement: .cancellationAction) {
                    Button(action: {
                        ueEditVM.onEdited(isCancelled: true)
                    }) {
                        Text("Cancel")
                    }
                }
                ToolbarItem(placement: .confirmationAction) {
                    Button(action: {
                        ueEditVM.onEdited()
                    }) {
                        Text("Done")
                    }
                }
            }
        }
    }
}

struct UEEdit_Previews: PreviewProvider {
    static var previews: some View {
        let odin = OdinVM(blocs: Stub().loadBlocs())
        let ueVM = odin.blocsVM[0].uesVM[0]
        
        UEEdit(ueEditVM: ueVM)
    }
}
