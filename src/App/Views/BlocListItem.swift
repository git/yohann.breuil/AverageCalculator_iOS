import SwiftUI
import Stub
import ViewModel

struct BlocListItem: View {
    @StateObject var odin: OdinVM
    
    var body: some View {
        VStack(alignment: .leading) {
            Label("Blocs", systemImage: "doc.on.doc.fill")
                .font(.title)
            Text("Vous devez avoir la moyenne à chacun de ces blocs pour avoir votre diplôme.")
            ForEach(odin.blocsVM) { blocVM in
                HStack {
                    Label("\(blocVM.name)", systemImage: "doc.on.doc.fill")
                    Spacer()
                    Text("\(String(format: "%.2f", blocVM.average))")
                    Image(systemName: "graduationcap.circle.fill")
                }
            }
        }
        .padding()
        .background(Color("ListItemBackgroundColor"))
        .cornerRadius(12)
    }
}

struct BlocListItem_Previews: PreviewProvider {
    static var previews: some View {
        let odinVM = OdinVM(blocs: Stub().loadBlocs())
        
        BlocListItem(odin: odinVM)
    }
}
