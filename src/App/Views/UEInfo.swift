import SwiftUI
import Stub
import ViewModel

struct UEInfo: View {
    @StateObject var ueVM: UEVM
        
    var body: some View {
        VStack {
            HStack() {
                Text("UE\(ueVM.number) \(ueVM.name)")
                Spacer()
                Text("\(ueVM.factor)")
            }
            UENoteSlider(ueVM: ueVM)
            Divider()
        }
    }
}

struct UEInfo_Previews: PreviewProvider {
    static var previews: some View {
        let odin = OdinVM(blocs: Stub().loadBlocs())
        let ueVM = odin.blocsVM[0].uesVM[0]
        
        UEInfo(ueVM: ueVM)
    }
}
