import SwiftUI
import Stub
import ViewModel

struct UENoteSlider: View {
    @StateObject var ueVM: UEVM
    
    @State private var sliderProgress: CGFloat = 0
    @State private var sliderWidth: CGFloat = 20
    @State private var sliderMaxWidth: CGFloat = 220
    @State private var sliderCurrentWidth: CGFloat = 0
        
    var drag: some Gesture {
        DragGesture()
            .onEnded { _ in
                sliderWidth = sliderWidth > sliderMaxWidth ? sliderMaxWidth : sliderWidth
                sliderWidth = sliderWidth >= 0 ? sliderWidth : 20

                sliderCurrentWidth = sliderWidth
            }
    }
    
    var body: some View {
        HStack {
            Rectangle()
                .fill(ueVM.average < 10  ? Color("BadColor") : Color("GoodColor"))
                .frame(width: CGFloat(ueVM.average) * 10, height: 25)
                .clipShape(Capsule())
                .gesture(drag, including: .none)
            Spacer()
            Text("\(String(format: "%.2f", ueVM.average))")
        }
    }
}

struct UENoteSlider_Previews: PreviewProvider {
    static var previews: some View {
        let odin = OdinVM(blocs: Stub().loadBlocs())
        let ueVM = odin.blocsVM[0].uesVM[0]
        
        UENoteSlider(ueVM: ueVM)
    }
}
