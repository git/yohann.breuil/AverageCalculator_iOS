import SwiftUI
import Stub
import ViewModel

struct MatiereNoteSlider: View {
    @ObservedObject var matiereVM: MatiereVM
    
    @State private var sliderProgress: CGFloat = 0
    @State private var sliderWidth: CGFloat = 20
    @State private var sliderMaxWidth: CGFloat = 200
    @State private var sliderCurrentWidth: CGFloat = 0
    
    var drag: some Gesture {
        DragGesture()
            .onChanged { value in
                let translation = value.translation.width
                sliderWidth = translation + sliderCurrentWidth
                
                sliderWidth = sliderWidth > sliderMaxWidth ? sliderMaxWidth : sliderWidth
                sliderWidth = sliderWidth >= 0 ? sliderWidth : 20
                
                let progress = sliderWidth / sliderMaxWidth * 20
                sliderProgress = progress <= 20 ? progress : 1
                
                matiereVM.note = Float(sliderProgress)
            }
            .onEnded { value in
                sliderWidth = sliderWidth > sliderMaxWidth ? sliderMaxWidth : sliderWidth
                sliderWidth = sliderWidth >= 0 ? sliderWidth : 20
                
                sliderCurrentWidth = sliderWidth
            }
    }
    
    var canEdit: Bool
    
    var body: some View {
        HStack {
            Rectangle()
                .fill(sliderProgress < 10  ? Color("BadColor") : Color("GoodColor"))
                .frame(width: sliderWidth, height: 25)
                .clipShape(Capsule())
                .gesture(drag, including: canEdit ? .gesture : .none)
            Spacer()
            Text("\(String(format: "%.2f", matiereVM.note))")
        }
    }
}

struct MatiereNoteSlider_Previews: PreviewProvider {
    static var previews: some View {
        let odin = OdinVM(blocs: Stub().loadBlocs())
        let matierevM = odin.blocsVM[0].uesVM[0].matieresVM[0]
        
        MatiereNoteSlider(matiereVM: matierevM, canEdit: true)
    }
}
