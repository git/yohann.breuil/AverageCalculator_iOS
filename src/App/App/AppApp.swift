import SwiftUI
import ViewModel
import Stub

@main
struct AppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(odin: OdinVM(blocs: Stub().loadBlocs()))
        }
    }
}
