import SwiftUI
import ViewModel
import Stub

struct ContentView: View {
    @StateObject var odin: OdinVM
    
    var body: some View {
        NavigationStack {
            ScrollView {
                BlocListItem(odin: odin)
                Divider()
                UEListItem(odin: odin)
            }
            .navigationTitle("Calculette")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let odinVM = OdinVM(blocs: Stub().loadBlocs())
        
        ContentView(odin: odinVM)
    }
}
