import Model
import Foundation

public struct Stub : DataManager {
    public init() {}
    
    public func loadBlocs() -> [Bloc] {
        var blocs: [Bloc] = []
        
        let bloc1 = Bloc(name: "Total", ues: [
            UE(number: 1, name: "Génie logiciel", factor: 6, matieres: [
                Matiere(name: "Processus de développement", factor: 4, note: 19.04),
                Matiere(name: "Programmation objets", factor: 9, note: 9.08),
                Matiere(name: "Qualité de développement", factor: 5, note: 13),
                Matiere(name: "Remise à niveau Objets", factor: 4, note: 12),
            ]),
            UE(number: 2, name: "Systèmes et réseaux", factor: 6, matieres: [
                Matiere(name: "Internet des Objets", factor: 4, note: 9),
                Matiere(name: "Réseaux", factor: 4, note: 8),
                Matiere(name: "Services Mobiles", factor: 4, note: 7),
                Matiere(name: "Système", factor: 5, note: 12)
            ]),
            UE(number: 3, name: "Insertion Professionnellle", factor: 6, matieres: [
                Matiere(name: "Anglais", factor: 5, note: 4),
                Matiere(name: "Économie", factor: 4, note: 1),
                Matiere(name: "Gestion", factor: 3, note: 1),
                Matiere(name: "Communication", factor: 4, note: 14)
            ]),
            UE(number: 4, name: "Technologies Mobiles 1", factor: 9, matieres: [
                Matiere(name: "Android", factor: 6, note: 2),
                Matiere(name: "Architecture de projets .NET (1)", factor: 5, note: 13),
                Matiere(name: "C++", factor: 4, note: 8),
                Matiere(name: "Swift", factor: 5, note: 8.5)
            ]),
            UE(number: 5, name: "Technologies Mobiles 2", factor: 9, matieres: [
                Matiere(name: "Architecture de projets .NET (2)", factor: 4, note: 12),
                Matiere(name: "Client / Serveur", factor: 4, note: 17),
                Matiere(name: "iOS", factor: 5, note: 20),
                Matiere(name: "Multiplateformes", factor: 3, note: 15),
                Matiere(name: "QT Quick", factor: 5, note: 1),
                Matiere(name: "Xamarin", factor: 5, note: 10)
            ]),
        ])
        
        let bloc2 = Bloc(name: "Projet / Stage", ues: [
            UE(number: 6, name: "Projet", factor: 9, matieres: [
                Matiere(name: "Projet", factor: 1, note: 12)
            ]),
            UE(number: 7, name: "Stage", factor: 15, matieres: [
                Matiere(name: "Stage", factor: 1, note: 15)
            ])
        ])
        
        blocs.append(contentsOf: [bloc1, bloc2])
        
        return blocs
    }
    
    public func saveBlocs(blocs: [Model.Bloc]) {
        
    }
}
