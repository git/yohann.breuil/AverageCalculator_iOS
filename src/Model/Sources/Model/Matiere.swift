import Foundation

public struct Matiere : Identifiable, Equatable {
    public let id: UUID
    
    public var name: String
    public var factor: Int
    public var note: Float
    
    public var realNote: Float {
        get {
            return note * Float(factor)
        }
    }
    
    public init(id: UUID, name: String, factor: Int, note: Float) {
        self.id = id
        self.name = name
        self.factor = factor
        self.note = note
    }
    
    public init(name: String, factor: Int, note: Float) {
        self.init(id: UUID(), name: name, factor: factor, note: note)
    }
    
    public static func == (lhs: Matiere, rhs: Matiere) -> Bool {
        return lhs.id == rhs.id
    }
}
