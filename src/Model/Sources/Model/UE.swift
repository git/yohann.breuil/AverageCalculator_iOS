import Foundation

public struct UE: Identifiable, Equatable {
    public let id: UUID
    
    public var number: Int32
    public var name: String
    public var factor: Int32
    public var matieres: [Matiere]
    
    public var average: Float {
        var averages : Float = 0
        var factors: Float = 0
        
        for matiere in matieres {
            averages = averages + (matiere.note * Float(matiere.factor))
            factors = factors + Float(matiere.factor)
        }
        
        return averages / factors
    }
    
    public init(id: UUID, number: Int32, name: String, factor: Int32, matieres: [Matiere]) {
        self.id = id
        self.number = number
        self.name = name
        self.factor = factor
        self.matieres = matieres
    }
    
    public init(number: Int32, name: String, factor: Int32, matieres: [Matiere]) {
        self.init(id: UUID(), number: number, name: name, factor: factor, matieres: matieres)
    }
    
    public mutating func addMatiere(matiere: Matiere) -> Void {
        matieres.append(matiere)
    }
    
    public mutating func addMatieres(matieres: Matiere...) -> Void {
        self.matieres.append(contentsOf: matieres)
    }
    
    public func findMatiereByName(name: String) -> Matiere? {
        if let index = matieres.firstIndex(where: { $0.name == name }) {
            return findMatiereByIndex(index: index);
        }
        return nil
    }
    
    public func findMatiereByIndex(index: Int) -> Matiere {
        return matieres[index];
    }
    
    public mutating func delMatiere(index: Int) -> Void {
        matieres.remove(at: index)
    }
    
    public static func == (lhs: UE, rhs: UE) -> Bool {
        return lhs.id == rhs.id
    }
}
