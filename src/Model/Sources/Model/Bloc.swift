import Foundation

public struct Bloc: Identifiable, Equatable {
    public let id: UUID
    
    public var name: String
    public var ues: [UE]
    
    public var average: Float {
        var averages : Float = 0
        var factors: Float = 0
        
        for ue in ues {
            averages = averages + (ue.average * Float(ue.factor))
            factors = factors + Float(ue.factor)
        }
        
        return averages / factors
    }
    
    public init(id: UUID, name: String, ues: [UE]) {
        self.id = id
        self.name = name
        self.ues = ues
    }
    
    public init(name: String, ues: [UE]) {
        self.init(id: UUID(), name: name, ues: ues)
    }
    
    public static func == (lhs: Bloc, rhs: Bloc) -> Bool {
        return lhs.id == rhs.id
    }
}
