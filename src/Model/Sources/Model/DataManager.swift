import Foundation

public protocol DataManager {
    func loadBlocs() -> [Bloc]
    
    func saveBlocs(blocs: [Bloc]) -> Void
}
