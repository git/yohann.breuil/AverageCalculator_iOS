import Foundation
import Model

@available(iOS 13.0, *)
public class UEVM : ObservableObject, Identifiable, Equatable {
    public init(model: UE) {
        self.model = model
    }
    
    private var copy: UEVM {
        UEVM(model: self.model)
    }
    
    public var editedCopy: UEVM?
    
    public var addedItem: MatiereVM?
    
    public var id: UUID { model.id }
    
    @Published
    var model: UE = UE(id: UUID(), number: 0, name: "", factor: 0, matieres: []) {
        didSet {
            if self.number != self.model.number {
                self.number = self.model.number
            }
            
            if self.name != self.model.name {
                self.name = self.model.name
            }
            
            if self.factor != self.model.factor {
                self.factor = self.model.factor
            }
            
            if !self.model.matieres.compare(to: self.matieresVM.map({$0.model})) {
                self.matieresVM = self.model.matieres.map({MatiereVM(model: $0)})
            }
            
            if self.average != self.model.average {
                self.average = self.model.average
            }
        }
    }
    
    @Published public var number: Int32 = 0 {
        didSet {
            if model.number != self.model.number {
                self.number = self.model.number
            }
        }
    }
    
    @Published public var name: String = "" {
        didSet {
            if model.name != self.model.name {
                self.name = self.model.name
            }
        }
    }
    
    @Published public var factor: Int32 = 0 {
        didSet {
            if model.factor != self.model.factor {
                self.factor = self.model.factor
            }
        }
    }
    
    @Published public var matieresVM: [MatiereVM] = [] {
        didSet {
            let matieres = self.matieresVM.map({$0.model})
            if !self.model.matieres.compare(to: matieres) {
                self.model.matieres = matieresVM.map({$0.model})
            }
        }
    }
    
    @Published public var average: Float = 0 {
        didSet {
            if self.average != self.model.average {
                self.average = self.model.average
            }
        }
    }
    
    @Published
    public var isEditing: Bool = false
    
    @Published
    public var isAdding: Bool = false
    
    public func onEditing(){
        editedCopy = self.copy
        isAdding = false
        isEditing = true
    }
    
    public func onAdding() {
        addedItem = MatiereVM()
        isAdding = true
        isEditing = false
    }
    
    public func onEdited(isCancelled cancel: Bool = false){
        if !cancel {
            if let edit = editedCopy {
                self.model = edit.model
            }
        }
        isEditing = false
    }
    
    public func onAdded(isCancelled cancel: Bool = false) {
        if !cancel {
            if let addedItem = addedItem {
                self.matieresVM.append(addedItem)
            }
        }
        addedItem = nil
        isAdding = false
    }
    
    public static func == (lhs: UEVM, rhs: UEVM) -> Bool {
        lhs.id == rhs.id
    }
}
