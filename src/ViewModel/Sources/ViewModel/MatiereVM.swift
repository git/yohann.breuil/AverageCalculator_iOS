import Foundation
import Model

@available(iOS 13.0, *)
public class MatiereVM : ObservableObject, Identifiable, Equatable {
    public init() {}
    
    public init(model: Matiere) {
        self.model = model
    }
    
    private var copy: MatiereVM {
        MatiereVM(model: self.model)
    }
    
    public var editedCopy: MatiereVM?
    
    public var id: UUID { model.id }
    
    @Published
    var model: Matiere = Matiere(id: UUID(), name: "", factor: 0, note: 0) {
        didSet {
            if self.name != model.name {
                self.name = self.model.name
            }
            
            if self.factor != model.factor {
                self.factor = self.model.factor
            }
            
            if self.note != model.note {
                self.note = self.model.note
            }
        }
    }
    
    @Published public var name: String = "" {
        didSet {
            if model.name != self.model.name {
                self.name = self.model.name
            }
        }
    }
    
    @Published public var factor: Int = 0 {
        didSet {
            if model.factor != self.model.factor {
                self.factor = self.model.factor
            }
        }
    }
    
    @Published public var note: Float = 0 {
        didSet {
            if model.note != self.model.note {
                self.note = self.model.note
            }
        }
    }
    
    @Published
    public var isEditing: Bool = false
    
    func onEditing(){
        editedCopy = self.copy
        isEditing = true
    }
    
    func onEdited(isCancelled: Bool = false){
        if(!isCancelled){
            if let edit = editedCopy {
                self.model = edit.model
            }
        }
        isEditing = false
    }
    
    public static func == (lhs: MatiereVM, rhs: MatiereVM) -> Bool {
        lhs.id == rhs.id
    }
}
