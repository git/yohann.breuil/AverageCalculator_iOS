import Foundation
import Model

@available(iOS 13.0, *)
public class BlocVM : ObservableObject, Identifiable, Equatable {
    public init(model: Bloc) {
        self.model = model
    }
    
    public var id: UUID { model.id }
    
    @Published var model: Bloc = Bloc(id: UUID(), name: "", ues: []) {
        didSet {
            if self.name != model.name {
                self.name = self.model.name
            }
            
            if !self.model.ues.compare(to: self.uesVM.map({$0.model})) {
                self.uesVM = self.model.ues.map({UEVM(model: $0)})
            }
            
            if self.average != self.model.average {
                self.average = self.model.average
            }
        }
    }
    
    @Published public var name: String = "" {
        didSet {
            if self.name != self.model.name {
                self.name = self.model.name
            }
        }
    }
    
    @Published public var uesVM: [UEVM] = [] {
        didSet {
            let ues = self.uesVM.map({$0.model})
            if !self.model.ues.compare(to: ues) {
                self.model.ues = uesVM.map({$0.model})
            }
        }
    }
    
    @Published public var average: Float = 0 {
        didSet {
            if self.average != self.model.average {
                self.average = self.model.average
            }
        }
    }
    
    public static func == (lhs: BlocVM, rhs: BlocVM) -> Bool {
        lhs.id == rhs.id
    }
}
