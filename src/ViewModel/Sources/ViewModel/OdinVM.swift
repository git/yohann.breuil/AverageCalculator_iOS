import Foundation
import Model

@available(iOS 13.0, *)
public class OdinVM: ObservableObject {
    @Published public var blocsVM: [BlocVM] = []
    
    public init(blocs: [Bloc]) {
        self.blocsVM = blocs.map({BlocVM(model: $0)})
    }
    
    public init(blocsVM: [BlocVM]) {
        self.blocsVM = blocsVM
    }
}
